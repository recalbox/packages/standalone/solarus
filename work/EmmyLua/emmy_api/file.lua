---@class file
---
---This module provides functions to manually read and write files from the quest data directory and from quest write directory.
---
local m = {}

_G.file = m

return m