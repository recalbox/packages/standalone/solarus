/**
\page lua_api_surface Surfaces

\tableofcontents

A surface is a 2D image. It is essentially a rectangle of pixels.
Its main feature is that you can draw objects on it.

\section lua_api_surface_functions Functions of sol.surface

\subsection lua_api_surface_create_empty sol.surface.create([width, height])

Creates an empty surface.
- \c width (number, optional): Width of the surface to create in pixels.
  The default value is the width of the logical screen.
- \c height (number, optional): Height of the surface to create in pixels.
  The default value is the height of the logical screen.
- Return value (surface): The surface created.

\subsection lua_api_surface_create_from_file sol.surface.create(file_name, [language_specific])

Creates a surface from an image file inside a predefined directory.
- \c file_name (string): Name of the image file to load, relative to either
  the \c sprites directory or the \c images subdirectory of the current
  language directory.
- \c language_specific (boolean, optional): \c true to load the image
  from the \c images subdirectory of the current language directory.
  The default is \c false and loads the image from the \c sprites directory.
- Return value (surface): The surface created, or \c nil if the image file
  could not be loaded.

\warning This method is deprecated since Solarus 2.0.

Use
\ref lua_api_surface_load "sol.surface.load()"
instead, which is safer and more general.

\subsection lua_api_surface_load sol.surface.load(file_name)

Creates a surface from an image file.

Generates a Lua error if the image could not be loaded.
- \c file_name (string): Name of the image file to load,
  relative to the quest data directory.
- Return value (surface): The surface created.

\section lua_api_surface_inherited_methods Methods inherited from drawable

Surfaces are particular \ref lua_api_drawable "drawable" objects.
Therefore, they inherit all methods from the type drawable.

See \ref lua_api_drawable_methods to know these methods.

\section lua_api_surface_methods Methods of the type surface

The following methods are specific to surfaces.

\subsection lua_api_surface_save surface:save(file_name)

Saves the content of the surface into a PNG file.

If the file already exists, it will be overwritten.
In case of failure, a Lua error is raised.
- \c file_name (string): Name of the file to save, including its .png extension,
  relative to the
  \ref lua_api_main_get_quest_write_dir "quest write directory".

\subsection lua_api_surface_get_size surface:get_size()

Returns the size of this surface.
- Return value 1 (number): Width of the surface in pixels.
- Return value 2 (number): Height of the surface in pixels.

\subsection lua_api_surface_clear surface:clear()

Erases all pixels of this surface.

All pixels become transparent.
The opacity property and the size of the surface are preserved.

\subsection lua_api_surface_fill_color surface:fill_color(color, [x, y, width, height])

Fills a region of this surface with a color.

If no region is specified, the entire surface is filled.
If the color has an alpha component different from 255 (opaque), then the
color is blended onto the existing pixels.
- \c color (table): The color as an array of 3 RGB values or 4 RGBA values
  (\c 0 to \c 255).
- \c x (number, optional): X coordinate of the region to fill on this surface.
- \c y (number, optional): Y coordinate of the region to fill on this surface.
- \c width (number, optional): Width of the region to fill on this surface.
- \c height (number, optional): Height of the region to fill on this surface.

\subsection lua_api_surface_get_pixels surface:get_pixels()

Returns all pixel values of this surface.
- Return value (string): The pixels as a pack of bytes in 32-bit RGBA format.
  Every pixel is stored on 4 bytes (red, green, blue, alpha).

\subsection lua_api_surface_set_pixels surface:set_pixels(pixels)

Sets all pixel values of this surface.

The number of pixels should match the size of the surface
(the size does not change).
- \c pixels (string): The pixels as a pack of bytes in 32-bit RGBA format.
  Every pixel is stored on 4 bytes (red, green, blue, alpha).

\subsection lua_api_surface_gl_bind_as_target surface:gl_bind_as_target()

Binds this surface as a target for next OpenGL calls.

This method is only useful for advanced users who want to directly call
OpenGL primitives,
which is possible with Lua bindings, for example via LuaJIT FFI.

This is equivalent to <tt>glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ...)</tt>.
It will make this surface the destination for any further OpenGL draw call.

When you call OpenGL directly, it is your responsability to restore the OpenGL
state if you want the Solarus rendering to remain correct.
In particular, pay attention to restore :
- The currently bound \c glProgram
- The current \c glBlendModes
- The currently bound \c VertexBufferObjects
- Face culling and alpha tests.

\remark Note that Solarus surfaces have no depth buffer attached.
A new framebuffer with depth attachement must be used
to benefit from \c GL_DEPTH_TEST.

\subsection lua_api_surface_gl_bind_as_texture surface:gl_bind_as_texture()

Bind this surface as the current texture for next OpenGL calls.

This method is only useful for advanced users who want to directly call
OpenGL primitives,
which is possible with Lua bindings, for example via LuaJIT FFI.

This is equivalent to <tt>glBindTexture(...)</tt>.

See \ref lua_api_surface_gl_bind_as_target for details about calling
OpenGL directly.

*/
