#------------------------------------------------------------------------------
# GitLab CI/CD Configuration for the Solarus Engine
#------------------------------------------------------------------------------
# This file configures a pipeline for building, testing and packaging the
# Solarus Engine. Packaging uses two jobs: staging-install and package.
# Artifacts from staging-install jobs can be used in other projects that
# depend on the Solarus Engine, e.g. the Solarus Quest Editor.
#
# There are three build targets currently supported:
#
# - Linux 64-bit (using gcc)
# - Windows 32-bit (using MinGW)
# - Windows 64-bit (using MinGW)
#
# Note that only the Windows targets generate redistributable packages.
#
# At the moment, only Linux-based testing is available. However, Windows
# staging-install and package jobs will not run if Linux testing fails.
#
# If a pipeline is started for the 'dev' or 'master' branches, a pipeline
# for the Solarus Quest Editor repository on the same branch will be
# triggered after the packaging jobs are completed.
#------------------------------------------------------------------------------

variables:
  IMAGE_REPOSITORY: registry.gitlab.com/solarus-games/solarus-devops
  IMAGE_VERSION: 20250217
  GIT_DEPTH: 10

stages:
  - build
  - test
  - install
  - package
  - trigger

#------------------------------------------------------------------------------
# Template for Generic Build Jobs
#------------------------------------------------------------------------------
# An artifact is generated with the contents of the `build` directory that
# expires one week after creation unless the pipeline is for the most recent
# commit on a branch or tag.
#
# Jobs can optionally define a `CMAKE_GEN_EXTRA_FLAGS` variable with extra
# flags to use in CMake during build system generation (default: unset).
#------------------------------------------------------------------------------

.build:
  stage: build
  before_script:
    - export CCACHE_BASEDIR=$CI_PROJECT_DIR
    - export CCACHE_DIR=$CI_PROJECT_DIR/ccache
    - export CCACHE_COMPILERCHECK=content
    - ccache --show-stats
  script:
    - $CMAKE_GEN_CMD -B build $CMAKE_GEN_FLAGS
    - $CMAKE_CMD --build build
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - build
    expire_in: 1 week
  cache:
    key: $CI_JOB_NAME-$CI_COMMIT_REF_SLUG
    paths:
      - ccache
  variables:
    CMAKE_GEN_CMD: cmake
    CMAKE_GEN_FLAGS: >-
      -DCMAKE_STAGING_PREFIX=$CI_PROJECT_DIR/staging
      -DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=TRUE
      -DCMAKE_C_COMPILER_LAUNCHER=ccache
      -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
      $CMAKE_GEN_EXTRA_FLAGS
    CMAKE_CMD: cmake

#------------------------------------------------------------------------------
# Template for Generic Install Jobs
#------------------------------------------------------------------------------
# An artifact is generated with the contents of the `staging` directory that
# expires one month after creation unless the pipeline is for the most recent
# commit on a branch or tag.
#------------------------------------------------------------------------------

.install:
  stage: install
  script:
    - $CMAKE_CMD --install build --strip
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - staging
    expire_in: 1 month
  variables:
    CMAKE_CMD: cmake

#------------------------------------------------------------------------------
# Template for MinGW-based Package Jobs
#------------------------------------------------------------------------------
# An artifact is generated with binaries, libraries and assets required to
# run the Solarus Engine that expires one month after creation unless the
# pipeline is for the most recent commit on a branch or tag.
#
# Jobs must define a `BINARIES` variable with a list of all binary objects
# to include in the package. Jobs can optionally define an `EXTRAS` variable
# with a list of additional files and directories to include in the package.
# Jobs can optionally define a `TEXT_FILES` variable with a list of text
# files to include in the root of the package.
#
# Jobs can optionally define a `BASE_DIR` variable with the name of the base
# directory to create inside the package (default: `solarus`). Jobs must
# define an `ARCH` variable with the MinGW target architecture for packaging.
# Possible values are `i686` (32-bit) or `x86_64` (64-bit). Jobs must also
# define a `PACKAGE_PREFIX` variable with a prefix for the package name.
#
# The generated packages are named `<package-prefix>-<commit-hash>.zip` for
# non-releases (based on commits) or `<package-prefix>-<tag-name>.zip` for
# releases (based on tags). Text files are newline-converted for Windows.
#------------------------------------------------------------------------------

.mingw-package:
  stage: package
  image: $IMAGE_REPOSITORY/mingw-build-env:$IMAGE_VERSION
  before_script:
    - PACKAGE_REF=${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}
    - PACKAGE=$PACKAGE_PREFIX-$PACKAGE_REF.zip
    - 'echo "Package: $PACKAGE"'
  script:
    - mingw_make_pkg.sh $ARCH $PACKAGE $BASE_DIR $BINARIES
    - if [[ -n $EXTRAS ]]; then pkg_add.sh $PACKAGE $BASE_DIR $EXTRAS; fi
    - if [[ -n $TEXT_FILES ]]; then
        unix2dos -k -o $TEXT_FILES;
        pkg_add.sh $PACKAGE . $TEXT_FILES;
      fi
    # START-HOTFIX: ensure that the LuaJIT library is used in the package
    - cp /usr/$ARCH-w64-mingw32/bin/luajit-2.1.dll lua51.dll
    - pkg_add.sh $PACKAGE $BASE_DIR lua51.dll
    # END-HOTFIX: will be properly fixed in mingw_make_pkg.sh later
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - $PACKAGE_PREFIX-*.zip
    expire_in: 1 month
  variables:
    BASE_DIR: solarus

#------------------------------------------------------------------------------
# Linux-based Build Job (64-bit)
#------------------------------------------------------------------------------

linux-build:
  extends: .build
  image: $IMAGE_REPOSITORY/linux-build-env:$IMAGE_VERSION
  variables:
    CMAKE_GEN_EXTRA_FLAGS: >-
      -DSOLARUS_TESTS_INSTALL=ON

#------------------------------------------------------------------------------
# Linux-based without LuaJIT Build Job (64-bit)
#------------------------------------------------------------------------------

linux-noluajit-build:
  extends: .build
  image: $IMAGE_REPOSITORY/linux-build-env:$IMAGE_VERSION
  variables:
    CMAKE_GEN_EXTRA_FLAGS: >-
      -DSOLARUS_TESTS_INSTALL=ON
      -DSOLARUS_USE_LUAJIT=OFF

#------------------------------------------------------------------------------
# MinGW-based Build Job (64-bit)
#------------------------------------------------------------------------------

mingw-x64-build:
  extends: .build
  image: $IMAGE_REPOSITORY/mingw-build-env:$IMAGE_VERSION
  variables:
    CMAKE_GEN_CMD: x86_64-w64-mingw32-cmake

#------------------------------------------------------------------------------
# MinGW-based without LuaJIT Build Job (64-bit)
#------------------------------------------------------------------------------

mingw-x64-noluajit-build:
  extends: .build
  image: $IMAGE_REPOSITORY/mingw-build-env:$IMAGE_VERSION
  variables:
    CMAKE_GEN_CMD: x86_64-w64-mingw32-cmake
    CMAKE_GEN_EXTRA_FLAGS: >-
      -DSOLARUS_USE_LUAJIT=OFF

#------------------------------------------------------------------------------
# MinGW-based Build Job (32-bit)
#------------------------------------------------------------------------------

mingw-x86-build:
  extends: .build
  image: $IMAGE_REPOSITORY/mingw-build-env:$IMAGE_VERSION
  variables:
    CMAKE_GEN_CMD: i686-w64-mingw32-cmake
  rules:
    - if: $CI_COMMIT_TAG

#------------------------------------------------------------------------------
# Documentation Build Job
#
# An artifact is generated with the contents of the `doc/build` directory that
# expires one month after creation unless the pipeline is for the most recent
# commit on a branch or tag.
#------------------------------------------------------------------------------

docs-build:
  stage: build
  image: $IMAGE_REPOSITORY/docs-build-env:$IMAGE_VERSION
  script:
    - cd doc/en
    - doxygen Doxyfile
    - cd ../build/latex
    - make
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - doc/build
    expire_in: 1 month

#------------------------------------------------------------------------------
# Linux-based Test Job (64-bit)
#------------------------------------------------------------------------------

linux-test:
  stage: test
  image: $IMAGE_REPOSITORY/linux-build-env:$IMAGE_VERSION
  dependencies:
    - linux-build
  script:
    - xvfb-run -a --server-args "-screen 0 640x480x24" -e /dev/stderr
        ctest --test-dir build -V

#------------------------------------------------------------------------------
# Linux-based Staging Install Job (64-bit)
#------------------------------------------------------------------------------

linux-install:
  extends: .install
  image: $IMAGE_REPOSITORY/linux-build-env:$IMAGE_VERSION
  dependencies:
    - linux-build

#------------------------------------------------------------------------------
# MinGW-based Staging Install Job (64-bit)
#------------------------------------------------------------------------------

mingw-x64-install:
  extends: .install
  image: $IMAGE_REPOSITORY/mingw-build-env:$IMAGE_VERSION
  dependencies:
    - mingw-x64-build

#------------------------------------------------------------------------------
# MinGW-based Staging Install Job (32-bit)
#------------------------------------------------------------------------------

mingw-x86-install:
  extends: .install
  image: $IMAGE_REPOSITORY/mingw-build-env:$IMAGE_VERSION
  dependencies:
    - mingw-x86-build
  rules:
    - if: $CI_COMMIT_TAG

#------------------------------------------------------------------------------
# MinGW-based Package Job (64-bit)
#------------------------------------------------------------------------------

mingw-x64-package:
  extends: .mingw-package
  dependencies:
    - mingw-x64-install
  variables:
    ARCH: x86_64
    PACKAGE_PREFIX: solarus-player-x64
    BINARIES: >-
      staging/bin/libsolarus.dll
      staging/bin/libsolarus-gui.dll
      staging/bin/solarus-run.exe
      staging/bin/solarus-launcher.exe
    EXTRAS: >-
      staging/share/solarus-gui/translations
      /usr/$ARCH-w64-mingw32/lib/qt6/plugins/platforms
      /usr/$ARCH-w64-mingw32/lib/qt6/plugins/styles
    TEXT_FILES: >-
      changelog.md
      license
      license-details.md
      cmake/win32/readme.txt

#------------------------------------------------------------------------------
# MinGW-based Minimal Package Job (64-bit)
#------------------------------------------------------------------------------

mingw-x64-min-package:
  extends: .mingw-package
  dependencies:
    - mingw-x64-install
  variables:
    ARCH: x86_64
    PACKAGE_PREFIX: solarus-minimal-x64
    BINARIES: >-
      staging/bin/libsolarus.dll
      staging/bin/solarus-run.exe
    TEXT_FILES: >-
      changelog.md
      license
      license-details.md
      cmake/win32/readme.txt
  rules:
    - if: $CI_COMMIT_TAG

#------------------------------------------------------------------------------
# MinGW-based Package Job (32-bit)
#------------------------------------------------------------------------------

mingw-x86-package:
  extends: .mingw-package
  dependencies:
    - mingw-x86-install
  variables:
    ARCH: i686
    PACKAGE_PREFIX: solarus-player-x86
    BINARIES: >-
      staging/bin/libsolarus.dll
      staging/bin/libsolarus-gui.dll
      staging/bin/solarus-run.exe
      staging/bin/solarus-launcher.exe
    EXTRAS: >-
      staging/share/solarus-gui/translations
      /usr/$ARCH-w64-mingw32/lib/qt6/plugins/platforms
      /usr/$ARCH-w64-mingw32/lib/qt6/plugins/styles
    TEXT_FILES: >-
      changelog.md
      license
      license-details.md
      cmake/win32/readme.txt
  rules:
    - if: $CI_COMMIT_TAG

#------------------------------------------------------------------------------
# MinGW-based Minimal Package Job (32-bit)
#------------------------------------------------------------------------------

mingw-x86-min-package:
  extends: .mingw-package
  dependencies:
    - mingw-x86-install
  variables:
    ARCH: i686
    PACKAGE_PREFIX: solarus-minimal-x86
    BINARIES: >-
      staging/bin/libsolarus.dll
      staging/bin/solarus-run.exe
    TEXT_FILES: >-
      changelog.md
      license
      license-details.md
      cmake/win32/readme.txt
  rules:
    - if: $CI_COMMIT_TAG

#------------------------------------------------------------------------------
# Trigger CI/CD pipeline for the Solarus Quest Editor
#
# Only the 'dev' or 'master' branches are used for this trigger.
#------------------------------------------------------------------------------

quest-editor-trigger:
  stage: trigger
  trigger:
    project: solarus-games/solarus-quest-editor
    branch: $CI_COMMIT_BRANCH
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^(?:dev|master)$/
